// console.log("JS DOM")

// [Section] : Document Object Model(DOM)
	// Allows us to access or modify the properties of an html element in a webpage
	// it is standard on how to get, change, add or delete HTML elements
	// we will be focusing only with DOM in terms of managing forms

	// For selecting HTML elements we will be using document.querySelector / getElementById
		// Syntax: document.querySelectorAll("html element")

		// CSS selectors
			// class selector (.);
			// id selector (#);
			// tag selector (html tags);
			// universal selector(*);
			// attribute selector([attribute]);
	// querySelectorAll
	let universalSelector = document.querySelectorAll("*");
	
	let singleUniversalSelector = document.querySelector("*");

	// querySelector
	console.log(universalSelector)
	console.log(singleUniversalSelector)

	let classSelector = document.querySelectorAll(".full-name")

	console.log(classSelector);

	let singleClassSelector = document.querySelector(".full-name")

	console.log(singleClassSelector)

	let tagSelector = document.querySelectorAll("input")

	console.log(tagSelector)

	let spanSelector  = document.querySelector("span[id]")

	console.log(spanSelector)

	// getElement
		let element = document.getElementById("fullName")

		console.log(element)

		element = document.getElementsByClassName("full-name")

		console.log(element)

// [Section] Event Listeners
	// whenever a user interacts with a webpage, this action is considered as an event
	// working with events is large part of creating interactivity in a web page
	// specific function that will be triggered if the event happen

	// The function us is "addEvenntListener", it takes two arguments
		// first argument a string identifying the even
		// second argument, function that the listener will trigger once the "specified event" occur.

	let txtFirstName = document.querySelector("#txt-first-name");
	let txtLastName = document.querySelector("#txt-last-name");


	// Add event listener

	txtFirstName.addEventListener("keyup",()=> {
		console.log(txtFirstName.value);
		spanSelector.innerHTML = `${txtFirstName.value} ${txtLastName.value}`

	})

	txtLastName.addEventListener("keyup",()=> {
		console.log(txtLastName.value);
		spanSelector.innerHTML = `${txtFirstName.value} ${txtLastName.value}`

	})

	// [Colors na figure out din sa wakas ]
	let colors = document.querySelector("#text-color")
	// console.log(color)

	colors.addEventListener("change", ()=> {
		let colorchange = colors.value

		spanSelector.style.color = colorchange
		
	})